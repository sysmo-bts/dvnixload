.TH DVNIXLOAD 1 "December 2008" "dvnixload-0.1.0" "User's Manual"

.SH NAME
dvnixload \- UBL and Application flasher for TI DaVinci platforms.

.SH SYNOPSIS
.B dvnixload
[\fIOPTION\fR]... UBL APPLICATION

.SH DESCRIPTION
\fBdvnixload\fR is a utility to program a UBL (first stage bootloader) and an application
(2nd stage bootloader, ex. U-Boot) in NAND flash memory over the serial port.

Both files UBL and APPLICATION are mandatory and must be in the ELF format. APPLICATION is
typically U-Boot. You don't have to manually specify the entry point for the UBL or
APPLICATION, everything is extracted automatically from the ELF headers.

The program will convert the ELF files into binary format (for the RBL) and S-record format
(for the UBL). But the UBL source code must be modified somewhat because the original UBL
expected to see the starting load address in the S7 record inside the S-record file, but
the S7 record is supposed to hold the entry point according to the Motorola S-record
specification, and this is what the standard GNU tools like objcopy will do (objcopy
is called by \fBdvnixload\fR).

.SH "OPTIONS"

.TP
\fB\-b\fR, \fB\-\-baudrate\fR=\fIBAUDRATE\fR
Serial port baud rate. Default is 115200 bps

.TP
\fB\-d\fR, \fB\-\-debug\fR=\fILEVEL\fR
Produce verbose debugging output. Valid levels are:
.RS
.PD 1
.TP
.B 0
display only error messages
.TP
.B 1
display warnings (default)
.TP
.B 2
display info messages
.TP
.B 3
display debug messages
.TP
.B 4
increase verbosity of debug messages
.TP
.B 5
increase verbosity of debug messages
.PD
.RE

.TP
\fB\-p\fR, \fB\-\-port\fR=\fIPORT\fR
Serial port device. Here are some examples of valid devices:

.RS
.PD 1
.TP
.B /dev/ttyS0
First serial port (default)
.TP
.B /dev/ttyS1
Second serial port
.TP
.B /dev/ttyUSB0
First USB serial port
.PD
.RE

.TP
\fB\-?\fR, \fB\-\-help\fR
Give this help list

.TP
\fB\-\-usage\fR
Give a short usage message

.TP
\fB\-V\fR, \fB\-\-version\fR
Print program version

.SH ENVIRONMENT VARIABLES
The program will honor the value of the CROSS_COMPILE environment variable
when calling external programs (objdump, objcopy, readelf). The default value
is set to "\fBarm\-linux\-\fR"

.SH LIMITATIONS
\fBdvnixload\fR currently support only writing to NAND flash.

.SH CREDITS
\fBdvnixload\fR was written by Hugo Villeneuve <hugo@hugovil.com>.

.SH COPYRIGHT
\fBdvnixload\fR is free; anyone may redistribute it to anyone under the terms
stated in the GNU General Public License. A copy of the license is included in
the \fBdvnixload\fR distribution. You can also browse it online at
.I http://www.gnu.org/copyleft/gpl.html
