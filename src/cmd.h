/*
 * cmd.h -- external commands definitions.
 *
 * Copyright (C) 2008 Hugo Villeneuve <hugo@hugovil.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef CMD_H
#define CMD_H 1

int run_popen(char *lineptr[], char *buffer, char *exe_str);

int run_cmd_get_string(char *cmd, char *output, ssize_t size);

int copy_file(const char *src, const char *dst);

int remove_file(const char *filename);

int check_source_files(int app_elf_format, const char *ubl, const char *app);

#endif /* CMD_H */
