/*
 * options.c -- functions for processing command-line options and arguments.
 *
 * Copyright (C) 2008 Hugo Villeneuve <hugo@hugovil.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include "config.h"

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <argp.h>

#include "common.h"
#include "serial.h"
#include "options.h"

#define START_APP_BLOCK_NUM 6

static struct options_t options;

/* Global variables needed by libcommon debug functions. */
int debug_level;

/* For error reporting functions. */
const char *package_name = PACKAGE_NAME;
const char *argp_program_version = PACKAGE_STRING;
const char *argp_program_bug_address = PACKAGE_BUGREPORT;

/* Program documentation. */
static const char doc[] = PACKAGE_NAME \
	" -- UBL and Application flasher for TI DaVinci platforms.";

/* How many arguments we accept. */
#define ARGS_COUNT 2

/* A description of the arguments we accept. */
static char args_doc[] = "UBL APPLICATION";

/* The options we understand. */
static struct argp_option my_options[] = {
	{"baudrate", 'b', "BAUDRATE", 0,
	 "Serial port baud rate. Default is 115200 bps." },
	{"debug", 'd', "LEVEL", 0,
	 "Set verbosity level of messages. Valid levels are 0 (errors only)" \
	 " to 5 (full debug)" },
	{"no-header", 'n', NULL, 0,
	 "Don't write application header" },
	{"port",     'p', "PORT",     0,  "Serial port device. Default is" \
	 " /dev/ttyS0." },
	{"test",    't', NULL,        0,
	 "Perform DDR2 memory testing" },
	{"writeblock", 'w', "BLOCKNUM", 0,
	 "Write application at block BLOCKNUM in flash memory. Default is 6" },
	{ 0 }
};

/* Parse a single option. */
static error_t
parse_opt(int key, char *arg, struct argp_state *state)
{
	switch (key) {
	case 'b':
		options.baud_rate_index = validate_baudrate(arg);
		if (options.baud_rate_index < 0)
			argp_error(state, "unsupported baud rate `%s'", arg);
		break;
	case 'd':
		if (decode_debug_option(arg) < 0)
			argp_error(state, "unsupported debug level `%s'", arg);
		break;
	case 'n':
		options.app_header = 0; /* Don't write app. header. */
		break;
	case 'p':
		hv_strncpy(options.port_string, arg, MAX_FILENAME_LENGTH);
		break;
	case 't':
		options.ddr_test = 1;
		break;
	case 'w':
		options.blocknum = atoi(arg);
		break;
	case ARGP_KEY_ARG:
		if (state->arg_num == 0)
			hv_strncpy(options.ubl, arg, MAX_FILENAME_LENGTH);
		else if (state->arg_num == 1)
			hv_strncpy(options.app, arg, MAX_FILENAME_LENGTH);
		else
			argp_error(state, "extra argument `%s'", arg);
		break;
	case ARGP_KEY_END:
		switch (state->arg_num) {
		case 0:
			argp_error(state,
				   "Missing UBL and application filenames");
			break;
		case 1:
			argp_error(state, "Missing application filename");
			break;
		default:
			/* Ok */
			break;
		}
		break;
	default:
		return ARGP_ERR_UNKNOWN;
	}

	return 0;
}

/* Our argp parser. */
static struct argp argp = {
	.options  = my_options,
	.parser   = parse_opt,
	.args_doc = args_doc,
	.doc      = doc,
};

/*******************************************************************************
 * Initializes the different options passed as arguments on the command line.
 ******************************************************************************/
struct options_t *
parse_command_line_options(int argc, char *argv[])
{
	/* Setting default values. */
	debug_level = LEVEL_INFO;
	options.ddr_test = 0;
	options.blocknum = START_APP_BLOCK_NUM;
	options.app_header = 1; /* Write application header */
	options.ubl[0] = '\0';
	options.app[0] = '\0';
	hv_strncpy(options.port_string, DEFAULT_UART_PORT, MAX_FILENAME_LENGTH);
	options.baud_rate_index = 6;

	/* Parse our arguments. */
	argp_parse(&argp, argc, argv, 0, 0, NULL);

	return &options;
}
