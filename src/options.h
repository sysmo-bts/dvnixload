/*
 * options.h
 *
 * Copyright (C) 2008 Hugo Villeneuve <hugo@hugovil.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef OPTIONS_H
#define OPTIONS_H 1

#include "common.h"

struct options_t {
	int baud_rate_index;
	int ddr_test;
	int blocknum;
	int app_header;
	char port_string[MAX_FILENAME_LENGTH];
	char ubl[MAX_FILENAME_LENGTH];
	char app[MAX_FILENAME_LENGTH];
};

struct options_t *
parse_command_line_options(int argc, char *argv[]);

#endif /* OPTIONS_H */
