/*
 * serial.h -- serial port definitions.
 *
 * Copyright (C) 2008 Hugo Villeneuve <hugo@hugovil.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef SERIAL_H
#define SERIAL_H 1

int
validate_baudrate(char *baud_rate_string);

int
serial_port_open(const char *port, const int baud_rate_index);

void
serial_port_close(void);

int
set_dtr(int dtr);

int
set_rts(int rts);

int
send_message(const char *msg);

int
send_number(u_int32_t number, int digits);

int
send_buffer(u_int8_t *data, ssize_t size_in_bytes);

int
send_buffer_bin(u_int8_t *data, ssize_t size_in_bytes);

int
send_ascii_data(u_int8_t *data, ssize_t size_in_bytes);

int
wait_for_message(const char *str1, const char *str2, const char *str3);

#endif /* SERIAL_H */
