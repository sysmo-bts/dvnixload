/*
 * utils.c -- Miscellaneous utilities such as error reporting.
 *
 * Copyright (C) 2008 Hugo Villeneuve <hugo@hugovil.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include "config.h"

#include <stdio.h>
#include <stdarg.h>
#include <unistd.h>  /* UNIX standard function definitions */
#include <string.h>

#include "common.h"

/* Defined in options.c */
extern int debug_level;

int
decode_debug_option(char *arg)
{
	char *endptr;

	if (arg == NULL) {
		debug_level = LEVEL_ERR;
	} else {
		debug_level = strtol(arg, &endptr, 0);

		if (*endptr != '\0')
			return -1;
	}

	if (debug_level > LAST_DEBUG_LEVEL)
		return -1;
	else
		return 0;
}

void
log_debug1(const char *format, ...)
{
  FILE *stream = stdout;

  if (debug_level >= LEVEL_DEBUG1) {
	  va_list ap;

	  /* Printing the name of the program first. */
	  fprintf(stream, "%s debug1:  ", PACKAGE_NAME);

	  va_start(ap, format);
	  vfprintf(stream, format, ap);
	  va_end(ap);

	  fprintf(stream, "\n");
  }
}

void
log_debug2(const char *format, ...)
{
  FILE *stream = stdout;

  if (debug_level >= LEVEL_DEBUG2) {
	  va_list ap;

	  /* Printing the name of the program first. */
	  fprintf(stream, "%s debug2:  ", PACKAGE_NAME);

	  va_start(ap, format);
	  vfprintf(stream, format, ap);
	  va_end(ap);

	  fprintf(stream, "\n");
  }
}

void
log_debug3(const char *format, ...)
{
  FILE *stream = stdout;

  if (debug_level >= LEVEL_DEBUG3) {
	  va_list ap;

	  /* Printing the name of the program first. */
	  fprintf(stream, "%s debug3:  ", PACKAGE_NAME);

	  va_start(ap, format);
	  vfprintf(stream, format, ap);
	  va_end(ap);

	  fprintf(stream, "\n");
  }
}

void
log_info(const char *format, ...)
{
	FILE *stream = stdout;

	if (debug_level >= LEVEL_INFO) {
		va_list ap;

		/* Printing the name of the program first. */
		fprintf(stream, "%s info:    ", PACKAGE_NAME);

		va_start(ap, format);
		vfprintf(stream, format, ap);
		va_end(ap);

		fprintf(stream, "\n");
	}
}

void
log_warn(const char *format, ...)
{
	FILE *stream = stderr;

	if (debug_level >= LEVEL_WARN) {
		va_list ap;

		/* Printing the name of the program first. */
		fprintf(stream, "%s warning: ", PACKAGE_NAME);

		va_start(ap, format);
		vfprintf(stream, format, ap);
		va_end(ap);

		fprintf(stream, "\n");
	}
}

void
log_fail(const char *format, ...)
{
	FILE *stream = stderr;
	va_list ap;

	/* Printing the name of the program first. */
	fprintf(stream, "%s error:   ", PACKAGE_NAME);

	va_start(ap, format);
	vfprintf(stream, format, ap);
	va_end(ap);

	fprintf(stream, "\n");
}

int
hv_strncpy(char *dest, const char *src, ssize_t n)
{
	dest[0] = '\0'; /* NULL string. */

	if (src == NULL) {
		log_warn("null src string");
		return 1;
	}

	if (strlen(src) >= n) {
		log_warn("src string too long");
		return 1;
	}

	strncpy(dest, src, n);

	/* Add the null-terminating character just in case 'src' is larger
	 * than 'dest'. */
	dest[n - 1] = '\0';

	return 0;
}
