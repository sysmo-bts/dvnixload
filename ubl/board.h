/*
 * board.h - board definitions
 *
 * Copyright (C) 2008 Hugo Villeneuve <hugo@hugovil.com>
 *
 * Based on TI DaVinci Flash and Boot Utilities, original copyright follows:
 *   Copyright 2008 Texas Instruments, Inc. <www.ti.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
 */

#ifndef _BOARD_H_
#define _BOARD_H_

#include "common.h"
#include "davinci.h"

#if defined(board_dvevm)
#  define PINMUX1_DEFAULT PINMUX1_UART0

#elif defined(board_sffsdr)
#  define PINMUX1_DEFAULT PINMUX1_UART0 | PINMUX1_UART1 | PINMUX1_I2C | \
	PINMUX1_ASP

#elif defined(board_das)
#  define PINMUX0_DEFAULT PINMUX0_VLYNQEN | VLYNQ_WIDTH_4
#  define PINMUX1_DEFAULT PINMUX1_UART0 | PINMUX1_UART2 | PINMUX1_I2C | \
	PINMUX1_SPI

#elif defined(board_dm355evm)
#  define PINMUX0_DEFAULT 0x00007F55 /* All Video Inputs */
#  define PINMUX1_DEFAULT 0x00145555 /* All Video Outputs */
#  define PINMUX2_DEFAULT 0x00000004 /* EMIFA */
#  define PINMUX3_DEFAULT 0x1BFF55FF /* SPI0, SPI1, UART1, I2C, SD0, SD1,
				      * ASP0, CLKOUTs */
#  define PINMUX4_DEFAULT 0x00000000 /* MMC/SD0 instead of MS, SPI0 */

#elif defined(board_minidas)
#  define PINMUX0_DEFAULT 0x00005C00 /* 8-bits video input, rest is GPIOs. */
#  define PINMUX1_DEFAULT 0x00430000 /* All GPIOs (temporary: no PWM1 for buzzer) */
#  define PINMUX2_DEFAULT 0x00000C0A /* EMIF A3:13, CE0 & CE1. */
#  define PINMUX3_DEFAULT 0x0B7BAAC0 /* SPI0, SPI1, UART1, UART2, I2C, SD0,
				      * CLKOUT1, CLKOUT2 */
#  define PINMUX4_DEFAULT 0x00000001 /* MMC/SD0 + SPI0_SDI */

/* Optional GPIO used as a status LED. Make sure to enable the corresponding
 * PINMUX bit. */
#define STATUS_LED    GPIO(71)
#define DSP1_PWR_ENA  GPIO(95)
#define DSP2_PWR_ENA  GPIO(94)
#define HDD_ENA       GPIO(96)
#define FULL_ENA      GPIO(68)
#define ALCOHOL_ENA   GPIO(73)
#define CAMERA_RESETn GPIO(72)
#define FAN           GPIO(81)
#define BUZZER        GPIO(80)
#define WIFI_RESETn   GPIO(79)
#define GPS_RESETn    GPIO(78)
#define CAN_RESETn    GPIO(77)
#define ATA_RESETn    GPIO(76)

#endif

#endif /* _BOARD_H_ */
