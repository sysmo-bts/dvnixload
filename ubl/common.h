/*
 * common.h - common definitions
 *
 * Copyright (C) 2008 Hugo Villeneuve <hugo@hugovil.com>
 *
 * Based on TI DaVinci Flash and Boot Utilities, original copyright follows:
 *   Copyright 2008 Texas Instruments, Inc. <www.ti.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
 */

#ifndef _COMMON_H_
#define _COMMON_H_

#include <stdint.h>
#include <stdbool.h>
#include <string.h> /* For size_t */

#include "board.h"

/* Return types */
#define E_PASS    0
#define E_FAIL    1
#define E_TIMEOUT 2

/* Define this to have more verbose NAND debug messages */
/* #define NAND_DEBUG 1 */

/* Define this to write a RAMP into NAND for debugging. */
/* #define NAND_DEBUG_WRITE_RAMP 1 */

#define UBL_VERSION_STR "HV-UBL v0.2.11"

/* Define this for bypassing the ECC check when reading from the NAND.
 * This is useful for debugging or during development. */
#define NAND_BYPASS_READ_PAGE_ECC_CHECK 1

#define MAGIC_NUMBER_MASK       0xFFFFFF00
#define MAGIC_NUMBER_VALID      0xA1ACED00

/* RBL magic numbers */
#define RBL_MAGIC_SAFE		0xA1ACED00 /* Describes UBL flash image type for
					    * RBL. */

/* UBL magic numbers */
#define UBL_MAGIC_BIN_IMG	0xA1ACED66 /* Describes binary flash image type
					    * for UBL. */
#define UBL_MAGIC_GZIP_IMG	0xA1ACED77 /* Describes gzipped binary flash
					    * image type for UBL. */

/* UBL commands */
#define UBL_CMD_FLASH_UBL_APP	0xA1ACEDCC /* Download UBL & application via
					    * UART and burn in flash. */
#define UBL_CMD_FLASH_DATA	0xA1ACEDCD /* Download data via UART and
					    * burn in flash (no header in flash). */
#define UBL_CMD_FLASH_ERASE	0xA1ACEDCE /* Erase the whole flash. */
#define UBL_CMD_RUN_APP		0xA1ACEDDD /* Load and run application via UART. */
#define UBL_CMD_DDR_TEST	0xA1ACEDEE /* Test DDR2 memory. */

/* Define maximum downloadable image size */
#define MAX_IMAGE_SIZE		0xC00000 /* 12 Mbytes */

struct nor_boot_t {
	uint32_t magicNum;
	uint32_t entryPoint;	
	uint32_t appSize;
	uint32_t ldAddress;	/* Starting RAM address where image is to copied - XIP Mode */
};

enum bootmode_t {
	NON_SECURE_NAND = 0, /* Non-secure NAND mode */
	NON_SECURE_NOR,      /* Non-secure NOR mode */
	UNKNOWN_MODE,        /* Unknown mode */
	NON_SECURE_UART      /* Non-secure UART mode */
};

#define ENDIAN_SWAP(a) (((a&0xFF)<<24)|((a&0xFF0000)>>8)|((a&0xFF00)<<8)|((a&0xFF000000)>>24))

/* Log functions */
#define log_fail(_x_)  uart_send_str_lf(_x_)
#define log_info(_x_)  uart_send_str_lf(_x_)
#define log_debug(_x_) uart_send_str_lf(_x_)

#define host_msg(_x_)  uart_send_str_lf(_x_)

#endif /* _COMMON_H_ */
