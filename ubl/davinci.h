/*
 * davinci.h - common DaVinci platform definitions
 *
 * Copyright (C) 2008 Hugo Villeneuve <hugo@hugovil.com>
 *
 * Based on TI DaVinci Flash and Boot Utilities, original copyright follows:
 *   Copyright 2008 Texas Instruments, Inc. <www.ti.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
 */

#ifndef _DAVINCI_H_
#define _DAVINCI_H_

#include "common.h"

#if defined(DM644x)
#include "dm644x.h"
#elif defined(DM35x)
#include "dm35x.h"
#endif

/* -------------------------------------------------------------------------- *
 *    System Control Module register structure - See sprue14.pdf, Chapter 10  *
 *       for more details.                                                    *
 * -------------------------------------------------------------------------- */ 
struct sys_module_regs_t {
#if defined(DM644x)
	uint32_t PINMUX[2];         //0x00
	uint32_t DSPBOOTADDR;       //0x08
	uint32_t SUSPSRC;           //0x0C
	uint32_t INTGEN;            //0x10
#elif defined(DM35x)
	uint32_t PINMUX[5];         //0x00
#endif
	uint32_t BOOTCFG;           //0x14
	uint32_t ARM_INTMUX;        //0x18 - ONLY ON DM35x
	uint32_t EDMA_EVTMUX;       //0x1C - ONLY ON DM35x
	uint32_t DDR_SLEW;          //0x20 - ONLY ON DM35x
	uint32_t CLKOUT;            //0x24 - ONLY ON DM35x
	uint32_t DEVICE_ID;         //0x28
	uint32_t VDAC_CONFIG;       //0x2C - ONLY ON DM35x
	uint32_t TIMER64_CTL;       //0x30 - ONLY ON DM35x
	uint32_t USBPHY_CTL;        //0x34
#if defined(DM644x)
	uint32_t CHP_SHRTSW;        //0x38
#elif defined(DM35x)
	uint32_t MISC;              //0x38
#endif
	uint32_t MSTPRI[2];         //0x3C
	uint32_t VPSS_CLKCTL;       //0x44
#if defined(DM644x)
	uint32_t VDD3P3V_PWDN;      //0x48
	uint32_t DDRVTPER;          //0x4C
	uint32_t RSVD2[8];          //0x50 
#elif defined(DM35x)
	uint32_t DEEPSLEEP;         //0x48
	uint32_t RSVD0;             //0x4C
	uint32_t DEBOUNCE[8];       //0x50
	uint32_t VTPIOCR;           //0x70
#endif
};

#define SYSTEM ((volatile struct sys_module_regs_t *) 0x01C40000)

/* -------------------------------------------------------------------------- *
 *    ARM Interrupt Controller register structure - See sprue26.pdf for more  *
 *       details.                                                             *
 * -------------------------------------------------------------------------- */
struct aintc_regs_t {
	uint32_t FIQ0;
	uint32_t FIQ1;
	uint32_t IRQ0;
	uint32_t IRQ1;
	uint32_t FIQENTRY;
	uint32_t IRQENTRY;
	uint32_t EINT0;
	uint32_t EINT1;
	uint32_t INTCTL;
	uint32_t EABASE;
	uint32_t RSVD0[2];
	uint32_t INTPRI0;
	uint32_t INTPRI1;
	uint32_t INTPRI2;
	uint32_t INTPRI3;
	uint32_t INTPRI4;
	uint32_t INTPRI5;
	uint32_t INTPRI6;
	uint32_t INTPRI7;
};

#define AINTC ((volatile struct aintc_regs_t *) 0x01C48000)

/* -------------------------------------------------------------------------- *
 *    PLL Register structure - See sprue14.pdf, Chapter 6 for more details.   *
 * -------------------------------------------------------------------------- */
struct pll_regs_t {
	uint32_t PID;
	uint32_t RSVD0[56];
	uint32_t RSTYPE;    /* 0x0E4 */
	uint32_t RSVD1[6];
	uint32_t PLLCTL;    /* 0x100 */
	uint32_t RSVD2[3];
	uint32_t PLLM;      /* 0x110 */
	uint32_t RSVD3;
	uint32_t PLLDIV1;   /* 0x118 */
	uint32_t PLLDIV2;
	uint32_t PLLDIV3;
	uint32_t RSVD4;
	uint32_t POSTDIV;   /* 0x128 */
	uint32_t BPDIV;
	uint32_t RSVD5[2];
	uint32_t PLLCMD;    /* 0x138 */
	uint32_t PLLSTAT;
	uint32_t ALNCTL;
	uint32_t DCHANGE;
	uint32_t CKEN;
	uint32_t CKSTAT;
	uint32_t SYSTAT;
	uint32_t RSVD6[3];
	uint32_t PLLDIV4;   /* 0x160 - Only on DM35x */
	uint32_t PLLDIV5;   /* 0x164 - Only on DM644x */
};

#define PLL1 ((volatile struct pll_regs_t *) 0x01C40800)
#define PLL2 ((volatile struct pll_regs_t *) 0x01C40C00)

#define DEVICE_PLLCTL_CLKMODE_MASK  0x00000100
#define DEVICE_PLLCTL_PLLEN_MASK    0x00000001
#define DEVICE_PLLCTL_PLLPWRDN_MASK 0x00000002
#define DEVICE_PLLCTL_PLLRST_MASK   0x00000008
#define DEVICE_PLLCTL_PLLDIS_MASK   0x00000010
#define DEVICE_PLLCTL_PLLENSRC_MASK 0x00000020

#define DEVICE_PLLCMD_GOSET_MASK    0x00000001
#define DEVICE_PLLSTAT_GOSTAT_MASK  0x00000001
#define DEVICE_PLLDIV_EN_MASK       0x00008000
#define DEVICE_PLLSTAT_LOCK_MASK    0x00000002

/* -------------------------------------------------------------------------- *
 *    Power/Sleep Ctrl Register structure - See sprue14.pdf, Chapter 7        * 
 *       for more details.                                                    *
 * -------------------------------------------------------------------------- */
struct psc_regs_t {
	uint32_t PID;         // 0x000
	uint32_t RSVD0[3];    // 0x004
	uint32_t GBLCTL;      // 0x010 - NOT ON DM35x
	uint32_t RSVD1;       // 0x014
	uint32_t INTEVAL;     // 0x018
	uint32_t RSVD2[9];    // 0x01C
	uint32_t MERRPR0;     // 0x040
	uint32_t MERRPR1;     // 0x044
	uint32_t RSVD3[2];    // 0x048
	uint32_t MERRCR0;     // 0x050
	uint32_t MERRCR1;     // 0x054
	uint32_t RSVD4[2];    // 0x058
	uint32_t PERRPR;      // 0x060
	uint32_t RSVD5;       // 0x064
	uint32_t PERRCR;      // 0x068
	uint32_t RSVD6;       // 0x06C
	uint32_t EPCPR;       // 0x070
	uint32_t RSVD7;       // 0x074
	uint32_t EPCCR;       // 0x078
	uint32_t RSVD8[33];   // 0x07C
	uint32_t RAILSTAT;    // 0x100 - NOT ON DM35x
	uint32_t RAILCTL;     // 0x104 - NOT ON DM35x
	uint32_t RAILSEL;     // 0x108 - NOT ON DM35x
	uint32_t RSVD9[5];    // 0x10C
	uint32_t PTCMD;       // 0x120
	uint32_t RSVD10;      // 0x124
	uint32_t PTSTAT;      // 0x128
	uint32_t RSVD11[53];  // 0x12C
	uint32_t PDSTAT0;     // 0x200
	uint32_t PDSTAT1;     // 0x204
	uint32_t RSVD12[62];  // 0x208
	uint32_t PDCTL0;      // 0x300
	uint32_t PDCTL1;      // 0x304
	uint32_t RSVD13[134]; // 0x308
	uint32_t MCKOUT0;     // 0x520
	uint32_t MCKOUT1;     // 0x524
	uint32_t RSVD14[182]; // 0x528
	uint32_t MDSTAT[41];  // 0x800
	uint32_t RSVD15[87];  // 0x8A4
	uint32_t MDCTL[41];   // 0xA00
};

#define PSC ((volatile struct psc_regs_t*) 0x01C41000)

#if defined(DM644x)
/* See TMS320DM6446 errata 1.3.22 */
#define PSC_PTSTAT_WORKAROUND_REG (*((volatile uint32_t*) 0x01C41A20))
#endif

#define PD0                 0

/* PSC constants */
#define LPSC_VPSS_MAST      0
#define LPSC_VPSS_SLV       1
#define LPSC_EDMACC         2
#define LPSC_EDMATC0        3
#define LPSC_EDMATC1        4
#if defined(DM644x)
#define LPSC_EMAC           5
#define LPSC_EMAC_MEM_CTL   6
#define LPSC_MDIO           7
#define LPSC_RESERVED0      8
#elif defined(DM35x)
#define LPSC_TIMER3         5
#define LPSC_SPI1           6
#define LPSC_MMC_SD1        7
#define LPSC_ASP1           8
#endif
#define LPSC_USB            9
#if defined(DM644x)
#define LPSC_ATA            10
#define LPSC_VLYNQ          11
#define LPSC_HPI            12
#elif defined(DM35x)
#define LPSC_PWM3           10
#define LPSC_SPI2           11
#define LPSC_RTO            12
#endif
#define LPSC_DDR2           13
#define LPSC_AEMIF	    14
#define LPSC_MMC_SD0        15
#if defined(DM644x)
#define LPSC_RESERVED1      16
#elif defined(DM35x)
#define LPSC_MEMSTK         16
#endif
#define LPSC_ASP0           17
#define LPSC_I2C            18
#define LPSC_UART0          19
#if defined(DM35x)
#define LPSC_UART1          20
#define LPSC_UART2          21
#define LPSC_SPIO           22
#define LPSC_PWM0           23
#define LPSC_PWM1           24
#define LPSC_PWM2           25
#endif
#define LPSC_GPIO           26
#define LPSC_TIMER0         27
#define LPSC_TIMER1         28
#if defined(DM35x)
#define LPSC_TIMER2         29
#define LPSC_SYSMOD         30
#endif
#define LPSC_ARM            31
#if defined(DM644x)
#define LPSC_DSP            39
#define LPSC_IMCOP          40
#elif defined(DM35x)
#define LPSC_VPSS_DAC       40
#endif

#define EMURSTIE_MASK       0x00000200

#define PSC_ENABLE          0x3
#define PSC_DISABLE         0x2
#define PSC_SYNCRESET       0x1
#define PSC_SWRSTDISABLE    0x0

/* -------------------------------------------------------------------------- *
 *    DDR2 Memory Ctrl Register structure - See sprue22b.pdf for more details.*
 * -------------------------------------------------------------------------- */
struct ddr_mem_ctl_regs_t {
	uint32_t RSVD0;
	uint32_t SDRSTAT;
	uint32_t SDBCR;
	uint32_t SDRCR;
	uint32_t SDTIMR;
	uint32_t SDTIMR2;
#if defined(DM644x)
	uint32_t RSVD1[2];
#elif defined(DM35x)
	uint32_t RSVD1;
	uint32_t SDBCR2;
#endif
	uint32_t PBBPR; /* 0x20 */
	uint32_t RSVD2[39];
	uint32_t IRR;   /* 0xC0 */
	uint32_t IMR;
	uint32_t IMSR;
	uint32_t IMCR;
	uint32_t RSVD3[5];
	uint32_t DDRPHYCR;
	uint32_t RSVD4[2];
#if defined(DM644x)
	uint32_t VTPIOCR; /* 0xF0 - In system control module for DM35x */
#endif
};

#define DDR ((volatile struct ddr_mem_ctl_regs_t *) 0x20000000)

#define DDR_TEST_PATTERN 0xA55AA55A

#define SDBCR_TIMUNLOCK    (1 << 15)

#if defined(DM644x)

#define DDRVTPR (*((volatile uint32_t*) 0x01C42030))

#define DDRPHYCR_DEFAULT      0x50006400 /* Default value with reserved fields */
#define DDRPHYCR_READLAT_MASK (0x7 << 0)
#define SDBCR_DEFAULT         0x00130000 /* Default value with reserved fields */

#elif defined(DM35x)
#define DDRPHYCR_DEFAULT      0x28006400 /* Default value with reserved fields */
#define DDRPHYCR_READLAT_MASK (0xF << 0)
#define SDBCR_DEFAULT         0x00170000 /* Default value with reserved fields */
#endif

/* -------------------------------------------------------------------------- *
 *    AEMIF Register structure - See sprue20a.pdf for more details.           *
 * -------------------------------------------------------------------------- */
struct emif_regs_t {
	uint32_t ERCSR;           // 0x00
	uint32_t AWCCR;           // 0x04
	uint32_t SDBCR;           // 0x08 - NOT ON DM35x
	uint32_t SDRCR;           // 0x0C - NOT ON DM35x
	uint32_t A1CR;            // 0x10
	uint32_t A2CR;            // 0x14
	uint32_t A3CR;            // 0x18 - NOT ON DM35x
	uint32_t A4CR;            // 0x1C - NOT ON DM35x
	uint32_t SDTIMR;          // 0x20 - NOT ON DM35x
	uint32_t DDRSR;           // 0x24 - NOT ON DM35x
	uint32_t DDRPHYCR;        // 0x28 - NOT ON DM35x
	uint32_t DDRPHYSR;        // 0x2C - NOT ON DM35x
	uint32_t TOTAR;           // 0x30 - NOT ON DM35x
	uint32_t TOTACTR;         // 0x34 - NOT ON DM35x
	uint32_t DDRPHYID_REV;    // 0x38 - NOT ON DM35x
	uint32_t SDSRETR;         // 0x3C - NOT ON DM35x
	uint32_t EIRR;            // 0x40
	uint32_t EIMR;
	uint32_t EIMSR;
	uint32_t EIMCR;
	uint32_t IOCTRLR;         // 0x50 - NOT ON DM35x
	uint32_t IOSTATR;         // 0x54 - NOT ON DM35x
	uint32_t RSVD0;
	uint32_t ONENANDCTL;      // 0x5C - ONLY ON DM35x  
	uint32_t NANDFCR;         // 0x60
	uint32_t NANDFSR;         // 0x64
	uint32_t RSVD1[2];
	uint32_t NANDF1ECC;       // 0x70
	uint32_t NANDF2ECC;       // 0x74
	uint32_t NANDF3ECC;       // 0x78 - NOT ON DM35x
	uint32_t NANDF4ECC;       // 0x7C - NOT ON DM35x
	uint32_t RSVD2;           // 0x80
	uint32_t IODFTECR;
	uint32_t IODFTGCR;
	uint32_t RSVD3;
	uint32_t IODFTMRLR;       // 0x90
	uint32_t IODFTMRMR;       // 0x94
	uint32_t IODFTMRMSBR;     // 0x98
	uint32_t RSVD4[5];
	uint32_t MODRNR;          // 0xB0
	uint32_t RSVD5[2];
	uint32_t NAND4BITECCLOAD; // 0xBC - ONLY ON DM35x  
	uint32_t NAND4BITECC1;    // 0xC0 - ONLY ON DM35x  
	uint32_t NAND4BITECC2;    // 0xC4 - ONLY ON DM35x  
	uint32_t NAND4BITECC3;    // 0xC8 - ONLY ON DM35x  
	uint32_t NAND4BITECC4;    // 0xCC - ONLY ON DM35x  
	uint32_t NANDERRADD1;     // 0xD0 - ONLY ON DM35x  
	uint32_t NANDERRADD2;     // 0xD4 - ONLY ON DM35x  
	uint32_t NANDERRVAL1;     // 0xD8 - ONLY ON DM35x  
	uint32_t NANDERRVAL2;     // 0xDC - ONLY ON DM35x
};

#if defined(DM644x)
#define AEMIF ((volatile struct emif_regs_t *) 0x01E00000)
#elif defined(DM35x)
#define AEMIF ((volatile struct emif_regs_t *) 0x01E10000)
#endif

/* -------------------------------------------------------------------------- *
 *    UART Register structure - See sprue33.pdf for more details.             *
 * -------------------------------------------------------------------------- */
struct uart_regs_t {
	uint32_t RBR;
	uint32_t IER;
	uint32_t IIR;
	uint32_t LCR;
	uint32_t MCR;
	uint32_t LSR;
	uint32_t MSR; /* NOT ON DM35x */
	uint32_t SCR; /* NOT ON DM35x */
	uint32_t DLL;
	uint32_t DLH;
	uint32_t PID1;
	uint32_t PID2;
	uint32_t PWREMU_MGNT;
};

#define THR RBR
#define FCR IIR

#define UART0 ((volatile struct uart_regs_t *) 0x01C20000)

#define UART_BCLK_RATIO	16	/* BCLK is 16 times the baudrate */
#define UART_BAUDRATE	115200

/* -------------------------------------------------------------------------- *
 *    Timer Register structure - See sprue26.pdf for more details.             *
 * -------------------------------------------------------------------------- */
struct timer_regs_t {
	uint32_t PID12;
	uint32_t EMUMGT_CLKSPD;
	uint32_t GPINT_GPEN;    // NOT ON DM35x
	uint32_t GPTDAT_GPDIR;  // NOT ON DM35x
	uint32_t TIM12;
	uint32_t TIM34;
	uint32_t PRD12;
	uint32_t PRD34;
	uint32_t TCR;
	uint32_t TGCR;
	uint32_t WDTCR;
	uint32_t RSVD1[3];       // 0x2C - ONLY ON DM35x
	uint32_t REL12;          // 0x34 - ONLY ON DM35x
	uint32_t REL34;          // 0x38 - ONLY ON DM35x
	uint32_t CAP12;          // 0x3C - ONLY ON DM35x
	uint32_t CAP34;          // 0x40 - ONLY ON DM35x
	uint32_t INTCTL_STAT;    // 0x44 - ONLY ON DM35x
};

#define TIMER0 ((volatile struct timer_regs_t *) 0x01C21400)

struct gpio_controller {
	uint32_t dir;
	uint32_t out_data;
	uint32_t set_data;
	uint32_t clr_data;
	uint32_t in_data;
	uint32_t set_rising;
	uint32_t clr_rising;
	uint32_t set_falling;
	uint32_t clr_falling;
	uint32_t intstat;
};

#define DAVINCI_GPIO_BASE 0x01C67000

#define GPIOC ((volatile struct gpio_controller *) DAVINCI_GPIO_BASE)

int davinci_platform_init(char *version);
void ddr_vtp_calibration(void);

void timer0_start(void);
uint32_t timer0_status(void);

#endif /* _DAVINCI_H_ */
