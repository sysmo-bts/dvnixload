/*
 * dm35x.c - DM35x specific platform initialization
 *
 * Copyright (C) 2008 Hugo Villeneuve <hugo@hugovil.com>
 *
 * Based on TI DaVinci Flash and Boot Utilities, original copyright follows:
 *   Copyright 2008 Texas Instruments, Inc. <www.ti.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
 */

#include "davinci.h"
#include "util.h"

#define DEVICE_VTPIOCR_PWRDN_MASK       (0x00000040)
#define DEVICE_VTPIOCR_LOCK_MASK        (0x00000080)
#define DEVICE_VTPIOCR_PWRSAVE_MASK     (0x00000100)
#define DEVICE_VTPIOCR_CLR_MASK         (0x00002000)
#define DEVICE_VTPIOCR_VTPIOREADY_MASK  (0x00004000)
#define DEVICE_VTPIOCR_READY_MASK       (0x00008000)

/* List of modules to enable in the PSC */
const int8_t lpsc_en_list[] = {
	LPSC_VPSS_MAST,
	LPSC_VPSS_SLV,
	LPSC_EDMACC,
	LPSC_EDMATC0,
	LPSC_EDMATC1,
	LPSC_TIMER3,
	LPSC_SPI1,
	LPSC_MMC_SD1,
	LPSC_ASP1,
	LPSC_USB,
	LPSC_PWM3,
	LPSC_SPI2,
	LPSC_RTO,
	LPSC_DDR2,
	LPSC_AEMIF,
	LPSC_MMC_SD0,
	LPSC_MEMSTK,
	LPSC_ASP0,
	LPSC_I2C,
	LPSC_UART0,
	LPSC_UART1,
	LPSC_UART2,
	LPSC_SPIO,
	LPSC_PWM0,
	LPSC_PWM1,
	LPSC_PWM2,
	LPSC_GPIO,
	LPSC_TIMER0,
	LPSC_TIMER1,
	LPSC_TIMER2,
	LPSC_SYSMOD,
	LPSC_ARM,
	LPSC_VPSS_DAC,
};

const size_t lpsc_en_list_len = sizeof(lpsc_en_list) /
	sizeof(lpsc_en_list[0]);

/* List of modules for which to control EMURSTIE */
const int8_t lpsc_emurstie_list[] = {
	LPSC_VPSS_MAST,
	LPSC_VPSS_SLV,
	LPSC_TIMER3,
	LPSC_SPI1,
	LPSC_USB,
	LPSC_PWM3,
	LPSC_DDR2,
	LPSC_AEMIF,
	LPSC_MMC_SD0,
	LPSC_ASP0,
	LPSC_GPIO,
	LPSC_VPSS_DAC,
};

const size_t lpsc_emurstie_list_len = sizeof(lpsc_emurstie_list) /
	sizeof(lpsc_emurstie_list[0]);

/* DDR2 VTP Calibration */
void
ddr_vtp_calibration(void)
{
	/* DO VTP calibration:
	 * Clear CLR & PWRDN & LOCK bits */
	SYSTEM->VTPIOCR &= ~(DEVICE_VTPIOCR_PWRDN_MASK |
			     DEVICE_VTPIOCR_LOCK_MASK |
			     DEVICE_VTPIOCR_CLR_MASK);

	/* Un-clear VTP */
	SYSTEM->VTPIOCR |= DEVICE_VTPIOCR_CLR_MASK;

	/* Wait for ready */
	while (!(SYSTEM->VTPIOCR & DEVICE_VTPIOCR_READY_MASK));

	/* Set bit VTP_IO_READY */
	SYSTEM->VTPIOCR |= DEVICE_VTPIOCR_VTPIOREADY_MASK;

	/* Enable power save mode and lock impedance */
	SYSTEM->VTPIOCR |= (DEVICE_VTPIOCR_PWRSAVE_MASK |
			    DEVICE_VTPIOCR_LOCK_MASK);

	/* Powerdown VTP as it is locked */
	SYSTEM->VTPIOCR |= DEVICE_VTPIOCR_PWRDN_MASK;

	/* Wait for calibration to complete */
	waitloop(150);
}
