/*
 * dm35x.h - DM35x specific platform definitions
 *
 * Copyright (C) 2008 Hugo Villeneuve <hugo@hugovil.com>
 *
 * Based on TI DaVinci Flash and Boot Utilities, original copyright follows:
 *   Copyright 2008 Texas Instruments, Inc. <www.ti.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
 */

#ifndef _DM35X_H_
#define _DM35X_H_

#include "common.h"

#define SYSTEM_CLK_HZ 24000000

#define VPBE_CLK_HZ   27000000

#define PLL1_Mult 144 /* 216 MHz */

#define PLL2_Mult 114
#define PLL2_Div1   2 /* Fixed */
#define PLL2_Div2  -1 /* PLLDIV2 not used */

#define DEVICE_MISC_PLL1POSTDIV_MASK	0x00000002
#define DEVICE_MISC_AIMWAITST_MASK	0x00000001
#define DEVICE_MISC_TIMER2WDT_MASK	0x00000010

#define UBL_IMAGE_SIZE 0x7800 /* 30 kB UBL (2Kb reserved for RBL stack) */

/* Global Memory Timing and PLL Settings */
static const uint8_t  DDR_NM = 1;	/* 16-bit bus width only on DM35x. */
static const uint8_t  DDR_PAGESIZE = 2;	/* 1024-word page size. */

#define DDR_PBBPR_PR_OLD_COUNT 0x000000FE;

/* PINMUX2 register bit values */
#define PINMUX2_EM_CLK     (1 << 11)
#define PINMUX2_EM_AVD     (1 << 10)
#define PINMUX2_EM_WAIT    (1 <<  9)
#define PINMUX2_EM_WE_OE   (1 <<  8)
#define PINMUX2_EM_CE1     (1 <<  7)
#define PINMUX2_EM_CE0     (1 <<  6)
#define PINMUX2_EM_D7_0    (1 <<  5)
#define PINMUX2_EM_D15_8   (1 <<  4)
#define PINMUX2_EM_BA0     (1 <<  2)
#define PINMUX2_EM_A0_BA1  (1 <<  1)
#define PINMUX2_EM_A13_3   (1 <<  0)

#endif /* _DM35X_H_ */
