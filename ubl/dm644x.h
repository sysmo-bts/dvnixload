/*
 * dm644x.h - DM644x specific platform definitions
 *
 * Copyright (C) 2008 Hugo Villeneuve <hugo@hugovil.com>
 *
 * Based on TI DaVinci Flash and Boot Utilities, original copyright follows:
 *   Copyright 2008 Texas Instruments, Inc. <www.ti.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
 */

#ifndef _DM644X_H_
#define _DM644X_H_

#include "common.h"

#define SYSTEM_CLK_HZ 27000000

#define PLL1_Mult 22 /* DSP=594 MHz ARM=297 MHz */

#define PLL2_Mult 24 /* DDRPHY=324 MHz DDRCLK=162 MHz */
#define PLL2_Div1 12
#define PLL2_Div2  2

#define UBL_IMAGE_SIZE 0x3800 /* 14 kB UBL (2Kb reserved for RBL stack) */

/* Global Memory Timing and PLL Settings */
static const uint8_t  DDR_NM = 0;	/* 32-bit bus width by default. */
static const uint8_t  DDR_PAGESIZE = 2;	/* 1024-word page size. */

/*
 * See TMS320DM6446 errata 2.1.2:
 *   A value of $20 should provide a good ARM (cache enabled)
 *   performance and still allow good utilization by the VPSS or other
 *   modules.
 */
#define DDR_PBBPR_PR_OLD_COUNT 0x00000020;

/* PINMUX0 register bit values */
#define PINMUX0_EMACEN   (1 << 31)
#define PINMUX0_HPIEN    (1 << 29)
#define PINMUX0_CFLDEN   (1 << 27)
#define PINMUX0_CWE      (1 << 26)
#define PINMUX0_LFLDEN   (1 << 25)
#define PINMUX0_LOEEN    (1 << 24)
#define PINMUX0_RGB888   (1 << 23)
#define PINMUX0_RGB666   (1 << 22)
#define PINMUX0_ATAEN    (1 << 17)
#define PINMUX0_HDIREN   (1 << 16)
#define PINMUX0_VLYNQEN  (1 << 15)
#define PINMUX0_VLSCREN  (1 << 14)
#define PINMUX0_VLYNQWD1 (1 << 13)
#define PINMUX0_VLYNQWD0 (1 << 12)

#define VLYNQ_WIDTH_1 (0 << 12)
#define VLYNQ_WIDTH_2 (1 << 12)
#define VLYNQ_WIDTH_3 (3 << 12) /* See TI SPRUE26A document. */
#define VLYNQ_WIDTH_4 (2 << 12) /* See TI SPRUE26A document. */

/* PINMUX1 register bit values */
#define PINMUX1_TIMIN (1 << 18)
#define PINMUX1_CLK1  (1 << 17)
#define PINMUX1_CLK0  (1 << 16)
#define PINMUX1_ASP   (1 << 10)
#define PINMUX1_SPI   (1 <<  8)
#define PINMUX1_I2C   (1 <<  7)
#define PINMUX1_PWM2  (1 <<  6)
#define PINMUX1_PWM1  (1 <<  5)
#define PINMUX1_PWM0  (1 <<  4)
#define PINMUX1_U2FLO (1 <<  3)
#define PINMUX1_UART2 (1 <<  2)
#define PINMUX1_UART1 (1 <<  1)
#define PINMUX1_UART0 (1 <<  0)

#endif /* _DM644X_H_ */
