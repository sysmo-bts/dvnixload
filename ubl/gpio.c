/*
 * gpio.c - GPIO handling
 *
 * Copyright (C) 2008 Hugo Villeneuve <hugo@hugovil.com>
 *
 * Based on davinci gpio code from the Linux kernel, original copyright follows:
 *   Copyright (c) 2006-2007 David Brownell
 *   Copyright (c) 2007, MontaVista Software, Inc. <source@mvista.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
 */

#include "common.h"
#include "davinci.h"
#include "gpio.h"

static struct gpio_controller *
gpio_to_controller(unsigned gpio)
{
	void *ptr;

	if (gpio < 32 * 1)
		ptr = (void *) DAVINCI_GPIO_BASE + 0x10;
	else if (gpio < 32 * 2)
		ptr = (void *) DAVINCI_GPIO_BASE + 0x38;
	else if (gpio < 32 * 3)
		ptr = (void *) DAVINCI_GPIO_BASE + 0x60;
	else if (gpio < 32 * 4)
		ptr = (void *) DAVINCI_GPIO_BASE + 0x88;
	else
		ptr = NULL;

	return ptr;
}

static inline uint32_t
gpio_mask(unsigned gpio)
{
	return 1 << (gpio % 32);
}

int
gpio_direction_in(unsigned gpio)
{
	volatile struct gpio_controller *g = gpio_to_controller(gpio);
	uint32_t mask = gpio_mask(gpio);

	g->dir |= mask;

	return 0;
}

int
gpio_direction_out(unsigned gpio, int initial_value)
{
	volatile struct gpio_controller *g = gpio_to_controller(gpio);
	uint32_t mask = gpio_mask(gpio);

	if (initial_value)
		g->set_data = mask;
	else
		g->clr_data = mask;

	g->dir &= ~mask;

	return 0;
}

void
gpio_set(unsigned gpio, int state)
{
	volatile struct gpio_controller *g = gpio_to_controller(gpio);
	uint32_t mask = gpio_mask(gpio);

	if (state)
		g->set_data = mask;
	else
		g->clr_data = mask;
}
