/*
 * nand.h - NAND flash definitions
 *
 * Copyright (C) 2008 Hugo Villeneuve <hugo@hugovil.com>
 *
 * Based on TI DaVinci Flash and Boot Utilities, original copyright follows:
 *   Copyright 2008 Texas Instruments, Inc. <www.ti.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
 */

#ifndef _NAND_H_
#define _NAND_H_

#include "common.h"
#include "davinci.h"

/* Define which blocks are valid for writing UBL and APP data */
#define START_UBL_BLOCK_NUM     1
#define END_UBL_BLOCK_NUM       5
#define START_APP_BLOCK_NUM     6

#define MAX_PAGE_SIZE	        (2048+64) /* Data bytes + spare area */

/* NAND descriptor expected by RBL when it loads UBL image. */
struct nand_image_descriptor_t {
	uint32_t magic;
	uint32_t entry_point;
	uint32_t size_in_pages;
	uint32_t block_num;
	uint32_t page_num;
	uint32_t load_address; /* Not used by RBL */
};


int nand_init(void);

int nand_erase_all(void);

int nand_read_page(uint32_t block, uint32_t page, uint8_t *dest);

int nand_write_prog(struct nand_image_descriptor_t *im_desc,
		    const uint8_t *src, size_t size);

/* Copy Application from NAND to RAM */
int nand_copy(uint32_t *jump_entry_point);

int nand_get_pages_per_block(void);

int nand_get_bytes_per_page(void);

#endif /* _NAND_H_ */
