/*
 * norboot.c - NOR boot mode functions
 *
 * Copyright (C) 2008 Hugo Villeneuve <hugo@hugovil.com>
 *
 * Based on TI DaVinci Flash and Boot Utilities, original copyright follows:
 *   Copyright 2008 Texas Instruments, Inc. <www.ti.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
 */

#include "common.h"
#include "nor.h"
#include "util.h"
#include "uart.h"

extern NOR_INFO gNorInfo;

/* Function to find out where the Application is and copy to DRAM */
int
nor_copy(uint32_t *jump_entry_point)
{
	volatile struct nor_boot_t *hdr = 0;
	volatile uint32_t *appStartAddr = 0;
	volatile uint32_t count = 0;
	volatile uint32_t *ramPtr = 0;
	uint32_t blkSize, blkAddress;

	if (NOR_Init() != E_PASS)
		return E_FAIL;
	    
	DiscoverBlockInfo((gNorInfo.flashBase + UBL_IMAGE_SIZE), &blkSize,
			  &blkAddress);
	
	hdr = (volatile struct nor_boot_t *) (blkAddress + blkSize);

	/* Check for valid magic number. */
	if ((hdr->magicNum & 0xFFFFFF00) != MAGIC_NUMBER_VALID) {
		log_fail("No valid header found");
	 	return E_FAIL;
	}

	/* Set the source address for copy */
	appStartAddr = (uint32_t *)(((uint8_t*) hdr) + sizeof(struct nor_boot_t));

	if (hdr->magicNum == UBL_MAGIC_BIN_IMG) {
		log_fail("Unsupported image format");
		return E_FAIL;
	}

	ramPtr = (uint32_t *) hdr->ldAddress;

	/* Copy data to RAM */
	memcpy(ramPtr, appStartAddr, hdr->appSize);

	/* Application was read correctly, so set entrypoint */
	*jump_entry_point = hdr->entryPoint;

 	return E_PASS;
}
